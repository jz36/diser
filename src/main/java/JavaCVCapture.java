import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class JavaCVCapture {
    public static void main(String[] args) throws Exception {
        Java2DFrameConverter converter = new Java2DFrameConverter();
        BufferedImage image = new BufferedImage(256, 256, 5);
        File imageToSave;
        Rectangle rectangle;
        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        grabber.start();
        Frame grabbedImage = grabber.grab();

        CanvasFrame canvasFrame = new CanvasFrame("Cam");
        rectangle = new Rectangle(grabbedImage.imageWidth / 4, grabbedImage.imageHeight / 4, grabbedImage.imageWidth * 3 / 4, grabbedImage.imageHeight * 3 / 4);
        canvasFrame.setCanvasSize(grabbedImage.imageWidth,grabbedImage.imageHeight);

        System.out.println("framerate = " + grabber.getFrameRate());
        grabber.setFrameRate(grabber.getFrameRate());
        int i = 0;
        while (canvasFrame.isVisible() && (grabbedImage = grabber.grab()) != null) {
            canvasFrame.showImage(grabbedImage);
            imageToSave = new File("new_img/a/a_" + ++i + ".jpg");

            imageToSave.mkdirs();
            Java2DFrameConverter.copy(grabbedImage, image, 1d, false, rectangle);
//            image = converter.getBufferedImage(grabbedImage);
            ImageIO.write(image, "jpg", imageToSave);
        }
        grabber.stop();
        canvasFrame.dispose();
    }
}
