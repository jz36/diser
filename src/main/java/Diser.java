import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.impl.ActivationReLU;
import org.nd4j.linalg.activations.impl.ActivationSoftmax;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Diser {
    public static void main(String[] args) throws IOException {
        int h_w = 64;
        int channels = 3;
        int rngseed = 123;
        Random randNumGen = new Random(rngseed);
        int batchSize = 128;
        int outputNum = 24;
        int numEpoch = 15;

//        File trainData = new File("/home/iz36/IdeaProjects/diser/new_img/working_img/32x32/");
//        File testData = new File("/home/iz36/IdeaProjects/diser/new_img/working_img/32x32_test/");

        File trainData = new File("/home/iz36/IdeaProjects/diser/new_img/working_img/64x64");
        File testData = new File("/home/iz36/IdeaProjects/diser/new_img/working_img/64x64_test");

        FileSplit train = new FileSplit(trainData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
        FileSplit test = new FileSplit(testData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);

        ParentPathLabelGenerator labelGenerator = new ParentPathLabelGenerator();

        ImageRecordReader recordReader = new ImageRecordReader(h_w, h_w, channels, labelGenerator);
        recordReader.initialize(train);

        DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);

        DataNormalization dataNormalization = new ImagePreProcessingScaler(0, 1);
        dataNormalization.fit(dataIter);
        dataIter.setPreProcessor(dataNormalization);


        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(rngseed)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new AdaDelta())
                .list()
                .layer(0, new SeparableConvolution2D.Builder(5, 5).nIn(3).stride(2, 2).activation(new ActivationReLU()).nOut(16).build())
                .layer(1, new SeparableConvolution2D.Builder(5, 5).stride(2, 2).nIn(16).nOut(32).build())
                .layer(2, new SeparableConvolution2D.Builder(5, 5).stride(2, 2).nIn(32).nOut(64).build())
                .layer(3, new SeparableConvolution2D.Builder(5, 5).stride(2, 2).nIn(64).nOut(128).build())
                .layer(4, new DropoutLayer.Builder().build())
                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nIn(h_w * 2)
                        .nOut(24)
                        .activation(new ActivationSoftmax())
                        .weightInit(WeightInit.XAVIER)
                        .build()
                ).setInputType(InputType.convolutional(h_w, h_w, channels))
                .build();
        conf.setPretrain(false);
        conf.setBackprop(true);
        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        model.setListeners(new ScoreIterationListener(10));

        for (int i = 0; i < numEpoch; i++) {
            model.fit(dataIter);
        }


        File locationToSave = new File("diser_test_model_64_new.zip");

        boolean saveUpdater = false;

        ModelSerializer.writeModel(model, locationToSave, saveUpdater);

        recordReader.reset();

    }
}
