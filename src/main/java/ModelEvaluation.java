import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class ModelEvaluation {

    public static void main(String[] args) throws IOException {
        MultiLayerNetwork model = null;
        File testData = new File("/home/iz36/IdeaProjects/diser/new_img/working_img/64x64_test/");
        Random randNumGen = new Random(123);


        FileSplit test = new FileSplit(testData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);

        DataNormalization dataNormalization = new ImagePreProcessingScaler(0, 1);
        try {
            model = ModelSerializer.restoreMultiLayerNetwork(new File("diser_test_model_64_new.zip"));
        } catch (
                IOException e) {
            e.printStackTrace();
        }

        ParentPathLabelGenerator labelGenerator = new ParentPathLabelGenerator();

        ImageRecordReader recordReader = new ImageRecordReader(64, 64, 3, labelGenerator);

        recordReader.initialize(test);
        DataSetIterator testIterator = new RecordReaderDataSetIterator(recordReader, 128, 1, 24);
        dataNormalization.fit(testIterator);
        testIterator.setPreProcessor(dataNormalization);
        Evaluation evaluation = new Evaluation(24);

        DataSet next;
        INDArray output;
        while (testIterator.hasNext()) {
            next = testIterator.next();
            output = model.output(next.getFeatures());
            evaluation.eval(next.getLabels(), output);
        }
        System.out.println(evaluation.stats(true, true));
    }

}
