import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class EvaluateTestImagesWithFx extends Application {

    private BorderPane mainPain;
    private SplitPane left, right, bottom;
    private Label realLetter, predictLetter;
    private Button nextImageButton, predictImageButton;
    private ImageView imageView;
    private TextField realLetterText, predictLetterText;
    private final String alfabet = "abcdefghiklmnopqrstuvwxy";
    private String currentLetter;
    private File currentFile;
    private String filesDirectory = "/home/iz36/IdeaProjects/diser/new_img/working_img/64x64_test/";
    private File[] filesInDirectory;
    private INDArray inputImage, outputImage;
    private MultiLayerNetwork model;
    private NativeImageLoader imageLoader;
    private DataNormalization scaler;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        imageLoader = new NativeImageLoader(64, 64, 3);
        scaler = new ImagePreProcessingScaler(0, 1);
        try {
            model = ModelSerializer.restoreMultiLayerNetwork(new File("diser_test_model_64.zip"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mainPain = new BorderPane();
        left = new SplitPane();
        right = new SplitPane();
        bottom = new SplitPane();
        left.setOrientation(Orientation.VERTICAL);
        right.setOrientation(Orientation.VERTICAL);
        bottom.setOrientation(Orientation.HORIZONTAL);
        realLetter = new Label("Real letter");
        predictLetter = new Label("Predict letter");
        nextImageButton = new Button("Next image");
        predictImageButton = new Button("Predict image");
        imageView = new ImageView();
        realLetterText = new TextField();
        predictLetterText = new TextField();
        mainPain.setLeft(left);
        mainPain.setRight(right);
        mainPain.setBottom(bottom);
        mainPain.setCenter(imageView);
        left.getItems().add(realLetter);
        left.getItems().add(realLetterText);
        right.getItems().add(predictLetter);
        right.getItems().add(predictLetterText);
        bottom.getItems().add(nextImageButton);
        bottom.getItems().add(predictImageButton);
        mainPain.autosize();
        addListenerToNextImageButton();

        imageView.setFitWidth(600);
        imageView.setFitHeight(400);

        //creating a Group object
        Group group = new Group(mainPain);

        //Creating a Scene by passing the group object, height and width
        Scene scene = new Scene(group, 1000, 600);

        //Setting the title to Stage.
        stage.setTitle("Sample Application");

        //Adding the scene to Stage
        stage.setScene(scene);

        //Displaying the contents of the stage
        stage.show();

    }

    public void addListenerToNextImageButton() {
        nextImageButton.setOnAction((arg1) -> {
            currentLetter = String.valueOf(alfabet.charAt(ThreadLocalRandom.current().nextInt(0, alfabet.length() - 1)));
            realLetterText.setText(currentLetter);
            filesInDirectory = new File(filesDirectory + currentLetter + "/").listFiles();
            currentFile = filesInDirectory[ThreadLocalRandom.current().nextInt(0, filesInDirectory.length + 1)];
            try {
                Image image = new Image(new FileInputStream(currentFile));
                imageView.setImage(image);
                inputImage = imageLoader.asMatrix(currentFile);
                scaler.transform(inputImage);
                outputImage = model.output(inputImage);
                System.out.println(outputImage.toString());
                System.out.println(Arrays.stream(model.predict(inputImage.get())).max().getAsInt());
                predictLetterText.setText(String.valueOf(alfabet.charAt(Arrays.stream(model.predict(inputImage.get())).max().getAsInt())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
